#!/bin/bash

get_max () {
awk -v n="$1" -F "," '{print $n}' datalist_temp | grep -o '[0-9]\+' | sort -nr | sed '1!d'
}

get_min () {
awk -v n="$1" -F "," '{print $n}' datalist_temp | grep -o '[0-9]\+' | sort -n | sed '1!d'
}

get_avg () {
awk -v n="$1" -F "," '{print $n}' datalist_temp | awk '{for(i=1;i<=NR;i++) t+=$i;} END{print t/NR}' | tr ',' '.'
}

final_output () {
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $logname
echo minimum,`(for ((num=1;num<=9;num=num+1)); do get_min "$num"; done) | awk 'BEGIN{ORS=","}{print}'`$HOME,`get_min "11"`M >> $logname
echo maximum,`(for ((num=1;num<=9;num=num+1)); do get_max "$num"; done) | awk 'BEGIN{ORS=","}{print}'`$HOME,`get_max "11"`M >> $logname
echo average,`(for ((num=1;num<=9;num=num+1)); do get_avg "$num"; done) | awk 'BEGIN{ORS=","}{print}'`$HOME,`get_avg "11"`M >> $logname
}

# Init variables
logloc=`readlink -f ${BASH_SOURCE}`
log_date=$(date +"metrics_%Y%m")
log_hour=`if [ "$(date +"%H")" -eq "00" ]; then echo 23; else expr $(date +"%H") - 1 | xargs printf "%02d"; fi`
log_day=`if [ "$(date +"%H")" -eq "00" ]; then expr $(date +"%d") - 1 | xargs printf "%02d"; else echo $(date +"%d"); fi`
log_pttrn="$log_date$log_day$log_hour*"
DIR="$HOME/log"
logname=$DIR/$(date +"metrics_agg_%Y%m%d%H").log

mkdir -p $DIR

find $HOME/log -type f -iname "$log_pttrn" > filelist_temp

if [ -s filelist_temp ]; then # cek kalo ada lognya ada atau nggak dari 'find'
while read -r line
do
echo `sed '2!d' $line` >> datalist_temp
done < filelist_temp
rm filelist_temp

final_output

(crontab -l; echo "0 * * * * bash $logloc") | awk '!x[$0]++'|crontab
rm datalist_temp
else
rm filelist_temp
fi
chmod 400 $logname