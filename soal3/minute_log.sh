#!/bin/bash
logloc=`readlink -f ${BASH_SOURCE}`

DIR="$HOME/log"

mkdir -p $DIR

logname=$DIR/$(date +"metrics_%Y%m%d%H%M%S").log

freem=`free -m | awk 'BEGIN{ORS=","}1 {if(NR != 1){for (i=2; i<=NF;i++) print $i}}'`
dush=`du -shm $HOME | awk '{print $1}'` 
#sementara menggunakan du -shm karena memiliki masalah pada unit memori
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $logname
echo "$freem$DIR","$dush"M"" >> $logname
(crontab -l; echo "* * * * * bash $logloc") | awk '!x[$0]++'|crontab
chmod 400 $logname
