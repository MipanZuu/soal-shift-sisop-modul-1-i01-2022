# soal-shift-sisop-modul-1-I01-2022

### Modul 1 Sisop 2022 I01 Formal Report

Group Members:
1. Denta Bramasta Hidayat (5025201116)
2. Muhammad Fatih Akbar (5025201117)
3. Muhammad Zufarrifqi Prakoso (5025201276)

## Important Links

- [Questions](https://docs.google.com/document/d/13lHX4hO09jf07y_JFv0BpwunFL-om9eiNFDlIooSW3o/edit "Soal Shift 1 S22")
- [Soal 1 Answer](https://gitlab.com/MipanZuu/soal-shift-sisop-modul-1-i01-2022/-/tree/main/soal1)
- [Soal 2 Answer](https://gitlab.com/MipanZuu/soal-shift-sisop-modul-1-i01-2022/-/tree/main/soal2)
- [Soal 3 Answer](https://gitlab.com/MipanZuu/soal-shift-sisop-modul-1-i01-2022/-/tree/main/soal3)

## Table of Content
- [soal-shift-sisop-modul-1-I01-2022](#soal-shift-sisop-modul-1-i01-2022)
    - [Modul 1 Sisop 2022 I01 Formal Report](#modul-1-sisop-2022-i01-formal-report)
  - [Important Links](#important-links)
  - [Table of Content](#table-of-content)
- [Soal1](#soal1)
  - [Question](#question)
  - [register.sh](#registersh)
  - [Explanation](#explanation)
    - [1A. Variable initialization](#1a-variable-initialization)
    - [1B. Password criteria](#1b-password-criteria)
    - [1C(i). Log user already exist error](#1ci-log-user-already-exist-error)
    - [1C(ii). Log registration succesful](#1cii-log-registration-succesful)
  - [main.sh](#mainsh)
    - [1C(iii). Log wrong password login attempt](#1ciii-log-wrong-password-login-attempt)
    - [1C(iv). Log succesful login](#1civ-log-succesful-login)
    - [1D. Main Script](#1d-main-script)
    - [(i). dl N](#i-dl-n)
      - [function_starting](#function_starting)
      - [If a zip file under the same name already exist](#if-a-zip-file-under-the-same-name-already-exist)
    - [(ii). att](#ii-att)
  - [Revisions Made](#revisions-made)
- [Soal 2](#soal-2)
  - [Question](#question-1)
  - [soal2_forensic_dapos.sh](#soal2_forensic_dapossh)
  - [Explanation](#explanation-1)
    - [Script input/argument](#script-inputargument)
    - [2A. Creating the directory](#2a-creating-the-directory)
    - [2B. Average requests per hour](#2b-average-requests-per-hour)
      - [reqeachhour](#reqeachhour)
      - [Calculating the average and write the output into ratarata.txt](#calculating-the-average-and-write-the-output-into-rataratatxt)
    - [2C. Most frequently occuring IP and its count](#2c-most-frequently-occuring-ip-and-its-count)
      - [mostcommonip](#mostcommonip)
      - [Splitting \$mostcommonip and writing the output into result.txt](#splitting-mostcommonip-and-writing-the-output-into-resulttxt)
    - [2D. Count requests with a curl user-agent](#2d-count-requests-with-a-curl-user-agent)
    - [2E. Print all IP address that request on 22 Jan, 2 o'clock](#2e-print-all-ip-address-that-request-on-22-jan-2-oclock)
    - [Running the script & Output](#running-the-script--output)
    - [Revisions Made](#revisions-made-1)
- [Soal 3](#soal-3)
  - [Question](#question-2)
  - [minute_log.sh](#minute_logsh)
  - [Explanation](#explanation-2)
    - [3A. Input metrics to log file named metrics_(YmdHms).log](#3a-input-metrics-to-log-file-named-metrics_ymdhmslog)
    - [3B. Run script for every minute](#3b-run-script-for-every-minute)
  - [aggregate_minutes_to_hourly_log.sh](#aggregate_minutes_to_hourly_logsh)
    - [3C. Make a script that can make an aggregate log file for every hour with the minimum, maximum and average metrics from every logfile from the past hour](#3c-make-a-script-that-can-make-an-aggregate-log-file-for-every-hour-with-the-minimum-maximum-and-average-metrics-from-every-logfile-from-the-past-hour)
    - [3D. Make log file so that only the user/owner can read it](#3d-make-log-file-so-that-only-the-userowner-can-read-it)
  - [Revisions Made](#revisions-made-2)

# Soal1

## Question

>Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

The question further explained that we need to help Han create a shell script that search and download photos with a registration and login system. Downloaded photos will be compressed into a .zip file which all fire are protected with password as the same to the current user. Addint to that, when the same user downloads another batch of photos at the same day, the batch would also be inserted into the .zip file. To do this we would need to help Han create to shell scripts:

1. register.sh
2. main.sh

## register.sh


Used for registering a user which includes a username and a password. The password requires certain criteria as stated in the question. User informations are to be stored in the **users/user.txt** file.

```bash
#!/bin/bash
info=$(date +"%m/%d/%y %H:%M:%S")
direct="users/user.txt"
fold=`dirname $direct`
flag=1

echo "create a new username and password"
echo "for password there is some criteria:"
echo "1. Password must atleast be 8 character long"
echo "2. Password must have an uppercase and lowercase character"
echo "3. Password must be Alphanumeric"
echo "4. Password cant be the same as username"


echo "Please enter your username: "
read username;

while [ $flag -eq 1 ]
do
echo "Enter your password: "
read -s password;

if [ ${#password} -lt 8 ]; then
	echo "Password must atleast be 8 character long"
elif ! [[ "$password" =~ [[:upper:]] ]] || ! [[ "$password" =~ [[:lower:]] ]]; then
	echo "Password must have an uppercase and lowercase character"
elif ! [[ "$password" =~ ^[[:alnum:]]+$ ]]; then
	echo "Password must be alphanumeric"
elif [[ "$password" == "$username" ]]; then
	echo "Password cant be the same as username"
else
	flag=0
fi
done

if [[ -f $direct ]]; then
	if grep -qF "$username" $direct; then
		echo "username already taken"
		echo $info "REGISTER: ERROR User already exists" >> log.txt
	else
		echo "REGISTER: INFO User $username registered successfully"
		echo $info "REGISTER: INFO User $username registered successfully" >> log.txt
		echo $username   $password >> $direct
	fi
else
        echo "REGISTER: INFO User $username registered successfully"
        echo $info "REGISTER: INFO User $username registered successfully" >> log.txt
        mkdir -p $fold && echo $username   $password >> $direct
fi
exit
```

## Explanation

### 1A. Variable initialization

Four variables are created: 
+ **\$info** store the current timestamp, which will be used for log.
+ **\$direct** store the direction where user information will be kept.
+ **\$fold** store the current directory of the script, will be use to create the directory where user.txt will be put.
+ **\$flag** will be used in checking password criteria.

```bash
info=$(date +"%m/%d/%y %H:%M:%S")
direct="users/user.txt"
fold=`dirname $direct`
flag=1
```

### 1B. Password criteria

There are four criteria that needed to be fullfilled in the password as seen in the script below. In addition to that, the password are hidden when typing by implementing **-s** in **read**. The while-loop prevents the person creating an account until all criteria of the password are met, in which `flag = 0` will stop the loop. 

```bash
while [ $flag -eq 1 ]
do
echo "Enter your password: "
read -s password;

if [ ${#password} -lt 8 ]; then
	echo "Password must atleast be 8 character long"
elif ! [[ "$password" =~ [[:upper:]] ]] || ! [[ "$password" =~ [[:lower:]] ]]; then
	echo "Password must have an uppercase and lowercase character"
elif ! [[ "$password" =~ ^[[:alnum:]]+$ ]]; then
	echo "Password must be alphanumeric"
elif [[ "$password" == "$username" ]]; then
	echo "Password cant be the same as username"
else
	flag=0
fi
done
```

> **output:**
> 
> ![](https://i.ibb.co/TbpyF3c/Screen-Shot-2022-03-04-at-22-39-59.png)

### 1C(i). Log user already exist error

If somone registers a user under a username that have already existed in **user.txt**, a `REGISTER: ERROR User already exists` will appear on the **log.txt** file. This is done by first checking wether **users/user.txt** exist, and then using `grep -qF` on **user.txt** we can check wether a match username exist. If a match is found, then report to **log.txt**

```bash
if [[ -f $direct ]]; then
	if grep -qF "$username" $direct; then
		echo "username already taken"
		echo $info "REGISTER: ERROR User already exists" >> log.txt
...
```

### 1C(ii). Log registration succesful

If it passes all conditionts from the if statement above, then it would be indicated as a succesful registration as a `$info REGISTER: INFO User $username registered successfully` will appear on the **log.txt** file and that the inputted usernam and password will be recorded into **user.txt**.

```bash
...
        else
		echo "REGISTER: INFO User $username registered successfully"
		echo $info "REGISTER: INFO User $username registered successfully" >> log.txt
		echo $username   $password >> $direct
	fi
else
        echo "REGISTER: INFO User $username registered successfully"
        echo $info "REGISTER: INFO User $username registered successfully" >> log.txt
        mkdir -p $fold && echo $username   $password >> $direct
fi
```

> **log.txt**
>
> ![](https://i.ibb.co/yQXwWvd/Screen-Shot-2022-03-04-at-22-42-40.png)

> **user.txt**
>
> ![](https://i.ibb.co/nLXKKzp/Screen-Shot-2022-03-04-at-22-43-02.png)

## main.sh

```bash
#!/bin/bash
function_starting () {
        while [ $i -le $N ]; do
                if [ $i -le 9 ]; then
                add="0"
                else
                add=""
                fi

                link=`curl -i https://loremflickr.com/320/240 -s | awk '/location/{print $2}'`
                lorem=https://loremflickr.com
		dot=".jpg"
		url=${lorem}${link:0:$((${#link} - 4 - 1))}${dot}
                # url=${lorem:0:-1}
		echo $url
                curl "$url" > $directory/PIC_${add}${i}.jpg
                # echo ""
                i=$(($i+1))
                done
                zip --password $password -r $directory.zip $directory
                rm -rf $directory
}

find_range () {
    	i=`ls $directory | sort -r | awk 'NR==1{x=substr($1,5,2);printf "%d\n",x}'`
    	i=`expr $i + 1`
    	N=`expr $N + $i - 1`
}


info=$(date +"%m/%d/%y %H:%M:%S")
info_date=$(date +"%Y-%m-%d")
direct="users/user.txt"
input1="dl"
input2="att"
i=1

echo "LOGIN!!"
echo "Please enter your username: "
read username;
echo "Please enter your password: "
read -s password;
if grep -qFw "$username $password" $direct
then
    echo $info "LOGIN: INFO User $username logged in" >> log.txt
    echo "LOGIN: INFO User $username logged in"
    echo "please choose what you want to do"
    echo "1. dl N (how many picture that you want to download)"
    echo "2. att (to count how many tries login sucessful or not from the user)"
    read answer
        if [ `echo $answer | awk '{print $1}'` == $input1 ]
        then
                N=`echo $answer | awk '{print $2}'`
                directory=$info_date"_"$username

                if [[ ! -f "$directory.zip" ]]
                then
                mkdir $directory
                function_starting

                else
                unzip -P $password $directory.zip
                find_range
                function_starting
                fi

        elif [ "$answer" == "$input2" ]
		then
                # awk -v user="$username" ' BEGIN { print "success user login and failed " }
                # $5 == user { ++n } || $9 == user{ ++i }
                # END { print "success login:", n, "time(s)." "\nfailed login:", i, "times(s)." }' log.txt

                awk -v user="$username" 'BEGIN {count=0}
                $6 == user || $10 == user {count++}
                END {print "Counting:", count-1}' log.txt
                fi

else
	echo "Login Failed!"
    echo $info "LOGIN: ERROR Failed login attempt on user $username" >> log.txt
fi
```

### 1C(iii). Log wrong password login attempt

If we want to login but the password is wrong then message in log _"LOGIN: ERROR Failed login attempt on user USERNAME"_ we grep the username and password from user.txt to match with login username and password.

```bash
if grep -qFw "$username $password" $direct
then
	## action of if condition
else
	echo "Login Failed!"
    echo $info "LOGIN: ERROR Failed login attempt on user $username" >> log.txt
fi
```

> **Output:**
>
>![](https://i.ibb.co/MnxJFSN/Screen-Shot-2022-03-04-at-23-01-19.png)

> **log.txt**
>
>![](https://i.ibb.co/WpM4pv3/Screen-Shot-2022-03-04-at-23-02-40.png)

### 1C(iv). Log succesful login

If username and password are matched with user.txt then login is successfully. and message in log.txt is "LOGIN: INFO User USERNAME logged in"

```bash
if grep -qFw "$username $password" $direct
then
    echo $info "LOGIN: INFO User $username logged in" >> log.txt
    echo "LOGIN: INFO User $username logged in"
else
	# else condition
fi
```

> **output:**
> 
> ![](https://i.ibb.co/rxJhfqP/Screen-Shot-2022-03-04-at-23-09-29.png)

> **log.txt**
> 
> ![](https://i.ibb.co/zFX7mwp/Screen-Shot-2022-03-04-at-23-10-55.png)

### 1D. Main Script

After a succesful login, the user can choose between two operation:

1. dl N (download an ***N*** amount of photos from "https://loremflickr.com/320/240")
2. att (display the number of failed or successfull login attempt from the current user)

### (i). dl N

After reading, we need to check wether the inputted command is either **dl** or **att**, in which for **dl** since it consist of two fields we would need to isolate the first field by using **awk** and check if it match with **dl**. if it does, create two variable, **\$N** which is the amount of photo needed, isolated from **\$answer**, and **\directory** which contain the name of the zip file later on. After, there are 2 paths, wether if the directory have not been created (`if [[ ! -f "$directory.zip" ]]`) or if the directory under the same name already exist.

```bash
if [ `echo $answer | awk '{print $1}'` == $input1 ]
        then
                N=`echo $answer | awk '{print $2}'`
                directory=$info_date"_"$username

                if [[ ! -f "$directory.zip" ]]
                then
                mkdir $directory
                function_starting

                else
                unzip -P $password $directory.zip
                find_range
                function_starting
                fi
```

#### function_starting

A function named **function_starting** is used for downloading these images and naming them **PIC_XX** from 00 until ***N*** incrementall by using a while loop. A snippet of **function_starting** as seen below is used to create a two digit leading zero number, by checking wether the current number **(\$i)** is less or equal to 9.

```bash
function_starting () {
while [ $i -le $N ]; do
                if [ $i -le 9 ]; then
                add="0"
                else
                add=""
                fi
...
```
in the next part of **function_starting**, a **curl** with an **-i** flag is used to fetch the response header of the url, since https://loremflickr.com/320/240 is not a direct link to a photo, as so an **awk** is also used to isolate the cache for the actual photo. Once the actual link is retrieved, another **curl** is used for downloading the photos, in which it will be stored in a directory as stated in **\$directory**.

```bash
...
                link=`curl -i https://loremflickr.com/320/240 -s | awk '/location/{print $2}'`
                lorem=https://loremflickr.com
                dot=".jpg"
                url=${lorem}${link:0:$((${#link} - 4 - 1))}${dot}
                # url=${lorem:0:-1}
                echo $url
                curl "$url" > $directory/PIC_${add}${i}.jpg
                # echo ""
                i=$(($i+1))
        done
...
```

After all photos have been downloaded into the directory, the directory would then be zipped using **zip** and password protect it with the same password as the current user.

```bash
... 
                zip --password $password -r $directory.zip $directory
                rm -rf $directory
}
```
#### If a zip file under the same name already exist

Since a zip file with the same name will alread contain some photos, we wouldn't want to overwrite it but rather add to it. As so a function named **find_range** is used to define the range for **function_starting** where the variable **\$i** (the starting point) starts, and where **\$N** (the end point) ends. This is done by isolating the number from the filename of the last photo and incrementing it by 1. And for the **\$N** we would just add the inputted **N** from `dl N` add that with **\$i** and decrement it by 1 to restore it.

> **For example:**<br>
> The last photo is named **PIC_08**, that means **function starting** should start at **PIC_09**. If we want to ad 10 more photos, that mean the it should end at **PIC_18**.

```bash
find_range () {
    	i=`ls $directory | sort -r | awk 'NR==1{x=substr($1,5,2);printf "%d\n",x}'`
    	i=`expr $i + 1`
    	N=`expr $N + $i - 1`
}
```

Since the directory is zipped, we would need to unzip it first, after which the **find_range** function is started, followed by the usual **function_starting**.

```bash
        else
        unzip -P $password $directory.zip
        find_range
        function_starting
        fi
```

> **output:**
> 
> ![](https://i.ibb.co/HKX3CzZ/Screen-Shot-2022-03-04-at-23-34-42.png)
> ![](https://i.ibb.co/gyqWDTc/Screen-Shot-2022-03-04-at-23-35-18.png)
> ![](https://i.ibb.co/5hSXycB/Screen-Shot-2022-03-04-at-23-35-24.png)
>
> The output when a zip file under the same name already exist (which is the photo above), use `dl 5` as input:
>
>![](https://i.ibb.co/VCFMX60/Screen-Shot-2022-03-04-at-23-41-54.png)

### (ii). att

Used for displaying the number of failed or succesful login attempts from the current user that is logged in.

```bash
elif [ "$answer" == "$input2" ]
	then
                # awk -v user="$username" ' BEGIN { print "success user login and failed " }
                # $5 == user { ++n } || $9 == user{ ++i }
                # END { print "success login:", n, "time(s)." "\nfailed login:", i, "times(s)." }' log.txt

                awk -v user="$username" 'BEGIN {count=0}
                $6 == user || $10 == user {count++}
                END {print "Counting:", count-1}' log.txt
                fi
```

An **awk** is used for counting all succesful or failed login attempts log in the **log.txt** file, where `$6 == user` is to check the succesful one and `$10 == user` is to check the failed one. Since the succesful register log is also captured, we decrement it by 1 to compensate this.

```bash
awk -v user="$username" 'BEGIN {count=0}
                $6 == user || $10 == user {count++}
                END {print "Counting:", count-1}' log.txt
```

> **Output:**
>
> ![](https://i.ibb.co/4WtYfWW/Screen-Shot-2022-03-04-at-23-46-57.png)
> 
> ![](https://i.ibb.co/BKtP9jB/Screen-Shot-2022-03-04-at-23-54-41.png)

## Revisions Made

+ Changed **%y** to **%Y** for the **\$info_date** variable to conform with the question around name formatting for the zip file.

# Soal 2

## Question

> Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidakbertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan.Dapos langsung membuka log website dan menemukan banyak request yang berbahaya.Bantulah Dapos untuk membaca log website https://daffa.info dengan membuat sebuah script awk benama **"soal2_forensic_dapos.sh"**.

The question further explained that, from the given log file **(log_website_daffainfo.log)** as an input the script **soal2_forensic_dapos.sh** must do the following things:

1. Make a directory named **forensic_log_website_daffainfo_log** in which all file ouput form the script will be put.
2. Find the average requests per hour, then put the result into a new file named **ratarata.txt**
3. Find the most frequently occurring IP address and how much request it have sent, then put the result into a new file named **result.txt**.
4. Find out how much request uses a **curl** user-agent, then append the result into **result.txt**.
5. Print out all IP addresses that happen to request on **22 January on 2 o'clock** and append it all into **result.txt**.

The make the output clear to read, the formatting for **ratarata.txt** and **result.txt** is as follow:

 **ratarata.txt**
> *Rata-rata serangan adalah sebanyak **rata_rata** requests per jam*

**result.txt**
> *IP yang paling banyak mengakses server adalah: **ip_address** sebanyak **jumlah_request** requests*
>
> *Ada **jumlah_req_curl** requests yang menggunakan curl sebagai user-agent*
>
> ***IP Address Jam 2 pagi***<br>
> ***IP Address Jam 2 pagi***<br>
> ***...***

## soal2_forensic_dapos.sh

```bash
#!/bin/bash

if [ -z "$1" ]
then
	echo "missing argument, please insert." 
	exit
else

logloc=${1}

# First Task
rm -r -f forensic_log_website_daffainfo_log
mkdir forensic_log_website_daffainfo_log

# Second Task
reqeachhour=`(awk 'BEGIN{FS="\":\""}  $2~/:00:00/{if (NR!=1) {print substr($2, 1, length($2)-6)}}' $logloc | awk -F ':' '{print $1":"$2-1}';awk 'BEGIN{FS="\":\""}  $2!~/:00:00/{if (NR!=1) {print substr($2, 1, length($2)-6)}}' $logloc ) | sort | uniq -c | awk '{print $1}'| sed 's/ /\n/g'`
numberofhour=`echo $reqeachhour | awk '{print NF}'`
sumhour=`echo $reqeachhour | awk '{for(i=1;i<=NF;i++) t+=$i; print t}'`
rata_rata=`expr $sumhour / $numberofhour`
echo "Rata-rata serangan adalah sebanyak $rata_rata requests per jam" > forensic_log_website_daffainfo_log/ratarata.txt

# Third Task
mostcommonip=`awk 'BEGIN{FS="\""} {if (NR!=1) {print $2}}' $logloc | sort | uniq -c | sort -snrk1,1 | awk '{if (NR==1) {print}}'`
ip_address=`echo $mostcommonip | awk '{print $2}'`
jumlah_request=`echo $mostcommonip | awk '{print $1}'`
echo "IP yang paling banyak mengakses server adalah: $ip_address sebanyak $jumlah_request requests" > forensic_log_website_daffainfo_log/result.txt
echo "" >> forensic_log_website_daffainfo_log/result.txt

# Fourth Task
jumlah_req_curl=`awk '/curl/ {if (NR!=1) {print}}' $logloc | awk 'END {print NR}'`
echo "Ada $jumlah_req_curl requests yang menggunakan curl sebagai user-agent" >> forensic_log_website_daffainfo_log/result.txt
echo "" >> forensic_log_website_daffainfo_log/result.txt

# Fifth Task
IP_Address_Jam_2_pagi=`awk -F'":"|"' '/22\/Jan\/2022:02/{if (NR!=1) {print $2}}' $logloc`
IP_Address_Jam_2_pagi=`echo "$IP_Address_Jam_2_pagi" | uniq`
echo "$IP_Address_Jam_2_pagi" >> forensic_log_website_daffainfo_log/result.txt
fi
```

## Explanation
### Script input/argument
```bash
if [ -z "$1" ]
then
	echo "missing argument, please insert." 
	exit
else

logloc=${1}

...

fi
```
Since it is not specified on how the log file should be inputted, we have decided to use the script argument (as **\$1**) in which the argument would be the address for the log file. to prevent the script from running without an argument, an if else statement implemented; in which the ***-z*** flag is used to check wether the argument **(\$1)** is empty or not; where it is empty, it would echo an error and exit the script, while if it is not it would move the argument to **\$logloc** variable and proceed to the main script.

### 2A. Creating the directory
```bash
rm -r -f forensic_log_website_daffainfo_log
mkdir forensic_log_website_daffainfo_log
```
An **rm -r -f** is used to remove the directory and its content forcibly in order to prevent difficulties if a directory with the same name have already been created, followed by a **mkdir** to create the **forensic_log_website_daffainfo_log** directory.

### 2B. Average requests per hour
#### reqeachhour
```bash
reqeachhour=`(awk 'BEGIN{FS="\":\""}  $2~/:00:00/{if (NR!=1) {print substr($2, 1, length($2)-6)}}' $logloc | awk -F ':' '{print $1":"$2-1}';awk 'BEGIN{FS="\":\""}  $2!~/:00:00/{if (NR!=1) {print substr($2, 1, length($2)-6)}}' $logloc ) | sort | uniq -c | awk '{print $1}'| sed 's/ /\n/g'`
```
For this part, a variable named **\$reqeachhour** is created where it contain how many requests are there for each hour:

![reqeachhour echo output](https://i.imgur.com/HXeFptP.png)

This is done by piping multiple command which is as below:

```bash
(awk 'BEGIN{FS="\":\""}  $2~/:00:00/{if (NR!=1) {print substr($2, 1, length($2)-6)}}' $logloc | awk -F ':' '{print $1":"$2-1}';awk 'BEGIN{FS="\":\""}  $2!~/:00:00/{if (NR!=1) {print substr($2, 1, length($2)-6)}}' $logloc )
```
The first are two similar awk that isolates the timestamp from the log while removing the minutes and seconds while also ignoring the header of the log. This is done making sure that it only take record thats not 1 `if (NR!=1)` and by taking the second field (i.e **\$2**) of the log where we define the field seperator as ***":"*** followed by a substring function that removes the last 6 characters. The only main difference in the two awk are the regex pattern they use, one have `$2~/:00:00/` that check wether if it's right in the hour mark and that it have another awk piped to it, while the other `$2!~/:00:00/` are for the others.  For the awk with `$2~/:00:00/` it will then be piped to another awk which is ` awk -F ':' '{print $1":"$2-1}'` which decrement the hour by one while maintaining the date. This is done in order to conform witht he hour range rule of the question. After all of this, the two are outputs are combined using `( ... ; ... )`

The combine output would then be piped to `sort | uniq -c | awk '{print $1}'| sed 's/ /\n/g'` where it would be sorted, counted for all matches, sorted again, and given a newline for each records.

#### Calculating the average and write the output into ratarata.txt
```bash
numberofhour=`echo $reqeachhour | awk '{print NF}'`
sumhour=`echo $reqeachhour | awk '{for(i=1;i<=NF;i++) t+=$i; print t}'`
rata_rata=`expr $sumhour / $numberofhour`
echo "Rata-rata serangan adalah sebanyak $rata_rata requests per jam" > forensic_log_website_daffainfo_log/ratarata.txt
```

Having **\$reqeachhour**, we can count how many hours are in **\$reqeachhour** by counting how many fields are in it by using `awk '{print NF}'` ; then for the sum, we use `awk '{for(i=1;i<=NF;i++) t+=$i; print t}'` where for each fields it will add their value into variable **t**, where once complete would output the sum. This is then used for **\$rata_rata** by dividing **\$sumhour** with **\$numberofhour** using **expr**, this is then followed by a redirected echo which create **ratarata.txt** and writes onto it.

### 2C. Most frequently occuring IP and its count
#### mostcommonip
```bash
mostcommonip=`awk 'BEGIN{FS="\""} {if (NR!=1) {print $2}}' $logloc | sort | uniq -c | sort -snrk1,1 | awk '{if (NR==1) {print}}'`
```

mostcommonip contains the most frequently occuring IP address and its count:

![mostcommonip echo](https://i.imgur.com/8dAooBe.png)

This is done by piping multiple command which includes awk. First is to isolate the IP adress field of the log by using awk; same as **\$reqeachhour** we can ignore the header using `if (NR!=1)`, and since we define the field seperator as ***"***, the IP addresses would be at the second field **(\$2)**. Then piping the output onto `sort | uniq -c | sort -snrk1,1` where it would sort, count, and then sort it descendingly as a numerical data and that we would only want to focus only on the first field which is its count. Lastlly, using `awk '{if (NR==1) {print}}'` we can isolates the first row since its the highest value.

#### Splitting \$mostcommonip and writing the output into result.txt
```bash
ip_address=`echo $mostcommonip | awk '{print $2}'`
jumlah_request=`echo $mostcommonip | awk '{print $1}'`
echo "IP yang paling banyak mengakses server adalah: $ip_address sebanyak $jumlah_request requests" > forensic_log_website_daffainfo_log/result.txt
echo "" >> forensic_log_website_daffainfo_log/result.txt
```

Having **\$mostcommonip**, we can now split the IP address and its count into different variables, in which after that we can create **result.txt** and write out the values, followed by appending an empty line.

### 2D. Count requests with a curl user-agent
```bash
jumlah_req_curl=`awk '/curl/ {if (NR!=1) {print}}' $logloc | awk 'END {print NR}'`
echo "Ada $jumlah_req_curl requests yang menggunakan curl sebagai user-agent" >> forensic_log_website_daffainfo_log/result.txt
echo "" >> forensic_log_website_daffainfo_log/result.txt
```
By utilizing awk regex pattern with `/curl/`, it will only print lines that contains the string "curl" within, then piping that onto another awk that count the number of records which is the amount of requests with curl. then write and append the value onto **result.txt** usinng `>>` redirect.

### 2E. Print all IP address that request on 22 Jan, 2 o'clock
```bash
IP_Address_Jam_2_pagi=`awk -F'":"|"' '/22\/Jan\/2022:02/{if (NR!=1) {print $2}}' $logloc`
IP_Address_Jam_2_pagi=`echo "$IP_Address_Jam_2_pagi" | uniq`
echo "$IP_Address_Jam_2_pagi" >> forensic_log_website_daffainfo_log/result.txt
```

Similar to the previous, utilize regex pattern with `/22\/Jan\/2022:02/` to conform with the timestamp format as so we would only get lines on the specified hour and date, followed by ignoring the header as previous, and print the second field which are the ip collumn, define by a field seperator of **"** and **":"**. since there are multiple lines of the same IP, the previous output then is inserted into **uniq** to remove duplicates. then write and append the value onto **result.txt** usinng `>>` redirect.

### Running the script & Output
>**Input:**
>![input script](https://i.imgur.com/Htefs06.png)

>**Outputs:**
>
> Directory and files created (on the most right):
> ![file tree using ranger](https://i.imgur.com/E6IgN3o.png)
>
> ratarata.txt:
> ![ratarata.txt in nano](https://i.imgur.com/2EgyiSb.png)
>
> result.txt:
> ![result.txt in nano](https://i.imgur.com/8imVHJl.png)

### Revisions Made
+ Revised point [2B](#2b-average-requests-per-hour) where before match hours by looking only hour, changed into **range rule** (a single hour is considered xx:00:01 - xx+1:00:00). Refer to commit [9605f023](https://gitlab.com/MipanZuu/soal-shift-sisop-modul-1-i01-2022/-/commit/9605f023786a71ca4b69aa2f2f782cb2c3043233 "Revised soal2/soal2_forensic_dapos.sh on hour range issue ")


# Soal 3

## Question
> Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

In this problem we need to run command `free -m` and `du -sh` and input the metrics into the log file with the defined format and that it can run automatically for every minute and every hour for the aggregate file. This problem comprises of 2 file shell:

1. minute_log.sh
2. aggregate_minutes_to_hourly_log,sh

## minute_log.sh

This script is used to fetch and record the metrics into a log file which is done for every minutes.

```bash
#!/bin/bash
logloc=`readlink -f ${BASH_SOURCE}`
DIR="$HOME/log"
mkdir -p $DIR
logname=$DIR/$(date +"metrics_%Y%m%d%H%M%S").log
freem=`free -m | awk 'BEGIN{ORS=","}1 {if(NR != 1){for (i=2; i<=NF;i++) print $i}}'`
dush=`du -shm $HOME | awk '{print $1}'` 
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $logname
echo "$freem$DIR","$dush"M"" >> $logname
(crontab -l; echo "* * * * * bash $logloc") | awk '!x[$0]++'|crontab
chmod 400 $logname
```

## Explanation

### 3A. Input metrics to log file named metrics_(YmdHms).log

First of all we would need to make a logfile with the defined format. So therefore we create or locate the directory where the log would be put **(/home/{user}/log)** and use `date +"metrics_%Y%m%d%H%M%S"` to make sure the name of the logfile would be as per defined in the question.

```bash
DIR="$HOME/log"
mkdir -p $DIR
logname=$DIR/$(date +"metrics_%Y%m%d%H%M%S").log
```
The `free -m` and `du -sh` command output contains headers in it, as so to isolate the metrics we can use **awk** where for the `free -m` we can ignore the first row and collumn with the addition of seperating each record with a coma with `BEGIN{ORS=","}1`, while for the `du -sh` we can just simply take the first field. These metrics would then be written into the logfile **(\$logname)**.

```bash
freem=`free -m | awk 'BEGIN{ORS=","}1 {if(NR != 1){for (i=2; i<=NF;i++) print $i}}'`
dush=`du -shm $HOME | awk '{print $1}'` 
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $logname
echo "$freem$DIR","$dush"M"" >> $logname
```

>**logfile result**
>
>![logfile](https://i.imgur.com/gSNwnaN.png)

### 3B. Run script for every minute

To automatically run the script for every minute, we can utilize **crontab** with the parameter of `* * * * *`.

```bash
logloc=`readlink -f ${BASH_SOURCE}`
(crontab -l; echo "* * * * * bash $logloc") | awk '!x[$0]++'|crontab
```
As so a new logfile would appear every minute:

![logeveryminute](https://i.imgur.com/3H82qfm.png)

## aggregate_minutes_to_hourly_log.sh
```bash
#!/bin/bash

get_max () {
awk -v n="$1" -F "," '{print $n}' datalist_temp | grep -o '[0-9]\+' | sort -nr | sed '1!d'
}

get_min () {
awk -v n="$1" -F "," '{print $n}' datalist_temp | grep -o '[0-9]\+' | sort -n | sed '1!d'
}that
}

final_output () {
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $logname
echo minimum,`(for ((num=1;num<=9;num=num+1)); do get_min "$num"; done) | awk 'BEGIN{ORS=","}{print}'`$HOME,`get_min "11"`M >> $logname
echo maximum,`(for ((num=1;num<=9;num=num+1)); do get_max "$num"; done) | awk 'BEGIN{ORS=","}{print}'`$HOME,`get_max "11"`M >> $logname
echo average,`(for ((num=1;num<=9;num=num+1)); do get_avg "$num"; done) | awk 'BEGIN{ORS=","}{print}'`$HOME,`get_avg "11"`M >> $logname
}

# Init variables
logloc=`readlink -f ${BASH_SOURCE}`
log_date=$(date +"metrics_%Y%m")
log_hour=`if [ "$(date +"%H")" -eq "00" ]; then echo 23; else expr $(date +"%H") - 1 | xargs printf "%02d"; fi`
log_day=`if [ "$(date +"%H")" -eq "00" ]; then expr $(date +"%d") - 1 | xargs printf "%02d"; else echo $(date +"%d"); fi`
log_pttrn="$log_date$log_day$log_hour*"
DIR="$HOME/log"
logname=$DIR/$(date +"metrics_agg_%Y%m%d%H").log

mkdir -p $DIR

find $HOME/log -type f -iname "$log_pttrn" > filelist_temp

if [ -s filelist_temp ]; then
while read -r line
do
echo `sed '2!d' $line` >> datalist_temp
done < filelist_temp
rm filelist_temp

final_output

(crontab -l; echo "0 * * * * bash $logloc") | awk '!x[$0]++'|crontab
rm datalist_temp
else
rm filelist_temp
fi
chmod 400 $logname
```

### 3C. Make a script that can make an aggregate log file for every hour with the minimum, maximum and average metrics from every logfile from the past hour

First we would need to locate all logs from the previous hour. To do that, we can use the **find** command with a certain pattern. We can use the hour and date for our pattern where the hour is decremented by 1; One problem is when the hour is at `00:00` where in that case we can substitute hour with 23, and date decremented by 1. Afterward, we can insert our pattern **(\$log_pttrn)** into the **find** command, where it would write out all logfile location into a temporary file.

```bash 
log_hour=`if [ "$(date +"%H")" -eq "00" ]; then echo 23; else expr $(date +"%H") - 1 | xargs printf "%02d"; fi`
log_day=`if [ "$(date +"%H")" -eq "00" ]; then expr $(date +"%d") - 1 | xargs printf "%02d"; else echo $(date +"%d"); fi`
log_pttrn="$log_date$log_day$log_hour*"

 ...

find $HOME/log -type f -iname "$log_pttrn" > filelist_temp
```

In the case that there are no logfile from the previous hour, an if statement with `-s filelist_temp` check that the **filelist_temp** is not empty before proceeding to the main part of the script. Inside, the the logfiles would be read by a while-loop that will extract all of the logfile's second line into **datalist_temp**. once completed, **filelist_temp** would be removed since it is not needed anymore.

```bash
if [ -s filelist_temp ]; then 
while read -r line
do
echo `sed '2!d' $line` >> datalist_temp
done < filelist_temp
rm filelist_temp
```
>**`filelist_temp` content:**
>
>![filelist](https://i.imgur.com/CkQch5h.png)

>**`datalist_temp` content:**
>
>![datalist](https://i.imgur.com/MAw9uE9.png)

To find minimum and maximum metrics from **datalist_temp** we can create a function named `get min` and `get_max` . In this function a field would be picked  from `datalist_temp` depending on the argument of the function, and then we use sort to sort the list in ascending order for finding min and descending order for max using `sort -n` or `sort -nr`. After that top line is isolated on the list of the sorted list using `sed '1!d'`.
```bash
get_max () {
awk -v n="$1" -F "," '{print $n}' datalist_temp | grep -o '[0-9]\+' | sort -nr | sed '1!d'
}

get_min () {
awk -v n="$1" -F "," '{print $n}' datalist_temp | grep -o '[0-9]\+' | sort -n | sed '1!d'
}
```
For finding the average, another function name **get_avg** is created. As before, a field would be picked in which after all records would be sum up from the list and then we divided by `NR` which is the number of records of the list.
```bash 
get_avg () {
awk -v n="$1" -F "," '{print $n}' datalist_temp | awk '{for(i=1;i<=NR;i++) t+=$i;} END{print t/NR}' | tr ',' '.'
}
```
In which all of the three mentioned functions are centralized in the **final_output** function. Each functions are put under a for-loop that runs for each fields except the 10th and 11th where they are seperate. this would then be written into the specified aggregate logfile, **\$logname**.

```bash
final_output () {
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $logname
echo minimum,`(for ((num=1;num<=9;num=num+1)); do get_min "$num"; done) | awk 'BEGIN{ORS=","}{print}'`$HOME,`get_min "11"`M >> $logname
echo maximum,`(for ((num=1;num<=9;num=num+1)); do get_max "$num"; done) | awk 'BEGIN{ORS=","}{print}'`$HOME,`get_max "11"`M >> $logname
echo average,`(for ((num=1;num<=9;num=num+1)); do get_avg "$num"; done) | awk 'BEGIN{ORS=","}{print}'`$HOME,`get_avg "11"`M >> $logname
}
```
Since all operations is finished, we can remove the `datalist_temp` file and in order for the script to run automaticaly for every hour we use crontab with    `0 * * * *` parameter *(which means for every zeroth minute)*.
```bash
logloc=`readlink -f ${BASH_SOURCE}`
(crontab -l; echo "0 * * * * bash $logloc") | awk '!x[$0]++'|crontab
rm datalist_temp
```
> **Aggregate log file content :**
>
> ![aggmetrics](https://i.imgur.com/h1AuZjN.png)

> **Result of crontab :**
> 
> ![crontabagg](https://i.imgur.com/8CdRpew.png) 

### 3D. Make log file so that only the user/owner can read it
We use **chmod** command so that logfile can only be read by the user only. This is used in both shell script, for both logfiles.
```
chmod 400 $logname
```
## Revisions Made
+ Removed `$HOME` from free -m on minute_log.sh so that no error occurs
